package com.bkim.musicsearchhw9.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.bkim.musicsearchhw9.dataclass.Albums;
import com.example.musicsearchhw9.R;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AlbumsAdapter extends ArrayAdapter<Albums> {

	private List<Albums> list;
	private Context context;
	private int resource;

	public AlbumsAdapter(Context context, int resource, List<Albums> objects) {
		super(context, resource, objects);

		this.context = context;
		this.list = objects;
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if( view == null ) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(resource, parent, false);
		}
		
		final ImageView sampleImage = (ImageView) view.findViewById(R.id.sampleImage);
		TextView title = (TextView) view.findViewById(R.id.title);
		TextView artist = (TextView) view.findViewById(R.id.artist);
		TextView genre = (TextView) view.findViewById(R.id.genre);
		TextView year = (TextView) view.findViewById(R.id.year);
		
		final Albums albums = list.get(position);
		//title artist genre year
		title.setText("Title: \n" + albums.getTitle());
		artist.setText("Artist: \n" + albums.getArtist());
		genre.setText("Genre: \n" + albums.getGenre());
		year.setText("Year: \n" + albums.getYear());
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				
				Bitmap bitmap = (Bitmap) msg.obj;
				int width = bitmap.getWidth();
				int height = bitmap.getHeight();
				
				int desiredWidth = sampleImage.getWidth();
				
				if( width < desiredWidth ) {
					while( true ) {
						if( width >= desiredWidth ) {
							break;
						}
						width *= 2;
						height *= 2;
					}
					
					bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
				}

				sampleImage.setImageBitmap((Bitmap) bitmap);
				sampleImage.setAdjustViewBounds(true);
			}
		};
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				msg.obj = bitmapFromURL(albums.getCover());
				handler.sendMessage(msg);
			}
		}).start();

		return view;
		// return super.getView(position, convertView, parent);
	}
	
	Bitmap bitmapFromURL(String address) {
	    Bitmap x;

	    URL url = null;
		try {
			url = new URL(address);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	    HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}

	    try {
			connection.connect();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    InputStream input = null;
		try {
			input = connection.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}

	    x = BitmapFactory.decodeStream(input);
	    return x;
	}
	
	public int size() {
		return list.size();
	}

}
