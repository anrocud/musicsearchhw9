package com.bkim.musicsearchhw9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bkim.musicsearchhw9.adapters.AlbumsAdapter;
import com.bkim.musicsearchhw9.adapters.ArtistsAdapter;
import com.bkim.musicsearchhw9.adapters.SongsAdapter;
import com.bkim.musicsearchhw9.dataclass.Albums;
import com.bkim.musicsearchhw9.dataclass.Artists;
import com.bkim.musicsearchhw9.dataclass.Songs;
import com.example.musicsearchhw9.R;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

public class MusicListActivity extends Activity implements OnClickListener, OnItemClickListener {

	final private String searchTypeDefinition[] = {
			"Artists", "Albums", "Songs"
	};
	private ArrayAdapter<?> _adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.music_list_activity);

	    final ListView listView = (ListView) findViewById(R.id.musicList);
		final ProgressDialog progressDialog = ProgressDialog.show(this, "Downloading data", "Downloading data");
		final Handler handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
			    
				boolean noResult = false;
				if( _adapter instanceof ArtistsAdapter ) {
					if( ((ArtistsAdapter) _adapter).size() == 0 ) {
						noResult = true;
					}
				} else if( _adapter instanceof AlbumsAdapter ) {
					if( ((AlbumsAdapter) _adapter).size() == 0 ) {
						noResult = true;
					}
				} else if( _adapter instanceof SongsAdapter ) {
					if( ((SongsAdapter) _adapter).size() == 0 ) {
						noResult = true;
					}
				}
				if( noResult ) {
					String[] item = {"no search result"};
					_adapter = new ArrayAdapter<String>(MusicListActivity.this, R.layout.no_result, item);
					listView.setOnItemClickListener(null);
				}
			    listView.setAdapter(_adapter);
				progressDialog.dismiss();
			}
		};
		new Thread(new Runnable() {
			@Override
			public void run() {
				Intent intent = getIntent();
				String searchText = intent.getStringExtra("searchText");
				String searchType = intent.getStringExtra("searchType");
				
			    _adapter = null;
			    
			    if( searchType.equalsIgnoreCase(searchTypeDefinition[0]) ) {
			    	_adapter = new ArtistsAdapter(MusicListActivity.this, R.layout.music_list_item_artists, getArtists(searchText, searchType));
			    } else if( searchType.equalsIgnoreCase(searchTypeDefinition[1]) ) {
			    	_adapter = new AlbumsAdapter(MusicListActivity.this, R.layout.music_list_item_albums, getAlbums(searchText, searchType));
			    } else if( searchType.equals(searchTypeDefinition[2]) ) {
			    	_adapter = new SongsAdapter(MusicListActivity.this, R.layout.music_list_item_songs, getSongs(searchText, searchType));
			    }
			    
			    handler.sendEmptyMessage(0);
			}
		}).start();
	    
	    listView.setOnItemClickListener(this);
	}
	
	private ArrayList<Artists> getArtists(String search, String searchType) {
		JSONArray resultArray = getJSONResultArray(search, searchType);

		ArrayList<Artists> result = new ArrayList<Artists>();
		try {
			for( int i = 0; i < resultArray.length(); ++i ) {
				JSONObject record = (JSONObject) resultArray.get(i);
				
				Artists artists = new Artists();
				
				String cover = record.getString("cover");
				if( cover.equalsIgnoreCase("na") ) {
					cover = "http://www-scf.usc.edu/~beomjun/hw8/noartist.png";
				}
				
				artists
					.setCover(cover)
					.setName(record.getString("name"))
					.setGenre(record.getString("genre"))
					.setYear(record.getString("year"))
					.setDetails(record.getString("details"));
				
				result.add(artists);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private ArrayList<Albums> getAlbums(String search, String searchType) {
		JSONArray resultArray = getJSONResultArray(search, searchType);

		ArrayList<Albums> result = new ArrayList<Albums>();
		try {
			for( int i = 0; i < resultArray.length(); ++i ) {
				JSONObject record = (JSONObject) resultArray.get(i);
				
				Albums albums = new Albums();
				
				String cover = record.getString("cover");
				if( cover.equalsIgnoreCase("na") ) {
					cover = "http://www-scf.usc.edu/~beomjun/hw8/noalbum.png";
				}
				
				albums
					.setCover(cover)
					.setTitle(record.getString("title"))
					.setArtist(record.getString("artist"))
					.setGenre(record.getString("genre"))
					.setYear(record.getString("year"))
					.setDetails(record.getString("details"));
				
				result.add(albums);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private ArrayList<Songs> getSongs(String search, String searchType) {
		JSONArray resultArray = getJSONResultArray(search, searchType);

		ArrayList<Songs> result = new ArrayList<Songs>();
		try {
			for( int i = 0; i < resultArray.length(); ++i ) {
				JSONObject record = (JSONObject) resultArray.get(i);
				
				Songs Songs = new Songs();
				
				String sampleImage = record.getString("sampleImage");
				if( sampleImage.equalsIgnoreCase("na") ) {
					sampleImage = "http://www-scf.usc.edu/~beomjun/hw8/nosong.png";
				}
				
				Songs
					.setSampleImage(sampleImage)
					.setSampleAddress(record.getString("sampleAddress"))
					.setTitle(record.getString("title"))
					.setPerformer(record.getString("performer"))
					.setComposer(record.getString("composer"))
					.setDetails(record.getString("details"));
				
				result.add(Songs);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	private JSONArray getJSONResultArray(String search, String searchType) {
		String strResult = getJSON(search, searchType);
		
		JSONObject rootObject = null;
		try {
			rootObject = new JSONObject(strResult);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		JSONArray resultArray = null;
		try {
			resultArray = (JSONArray) ((JSONObject) rootObject.get("results")).get("result");
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		return resultArray;
	}

	private String getJSON(String search, String searchType) {
		searchType = searchType.toLowerCase();
		String urlString = "http://cs-server.usc.edu:28342/examples/servlet/musicsearch?";
		try {
			search = URLEncoder.encode(search, "UTF-8");
		} catch (UnsupportedEncodingException e2) {
			e2.printStackTrace();
		}
		urlString += "title=" + search;
		urlString += "&";
		urlString += "type=" + searchType;
		URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		URLConnection urlConnection = null;
		try {
			urlConnection = url.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		urlConnection.setAllowUserInteraction(false);
		InputStream urlStream = null;
		try {
			urlStream = url.openStream();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(urlStream));
		String buffer = null;
		String result = "";
		try {
			while( (buffer = br.readLine()) != null ) {
				result += buffer;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			br.close();
			urlStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		return result;
	}
	
	private Dialog _dialog;
	private Button _buttonPlayPause;
	private MediaPlayer _mediaPlayer;
	private int _playLength;
	private final Handler _handler = new Handler();

	@Override
	public void onClick(View v) {
		if( v.getId() == R.id.sampleMusicButton ) {
			LinearLayout layout = (LinearLayout) _dialog.findViewById(R.id.musicLinearLayout);
			if( v.getTag() == null ) {
				layout.setVisibility(View.VISIBLE);
				v.setTag(layout);
				
				_buttonPlayPause = (Button)_dialog.findViewById(R.id.pauseButton);
				_buttonPlayPause.setOnClickListener(this);
				
				final SeekBar seekBarProgress = (SeekBar)_dialog.findViewById(R.id.musicSeekBar);	
				seekBarProgress.setMax(99); // It means 100% .0-99
				seekBarProgress.setOnTouchListener(new OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(_mediaPlayer.isPlaying()){
					    	SeekBar sb = (SeekBar)v;
							int playPositionInMillisecconds = (_playLength / 100) * sb.getProgress();
							_mediaPlayer.seekTo(playPositionInMillisecconds);
						}
						return false;
					}
				});
				
				//_mediaPlayer = new MediaPlayer();
				_mediaPlayer.setOnBufferingUpdateListener(new OnBufferingUpdateListener() {
					@Override
					public void onBufferingUpdate(MediaPlayer mp, int percent) {
						seekBarProgress.setSecondaryProgress(percent);
					}
				});
			} else {
				layout.setVisibility(View.GONE);
				v.setTag(null);
			}
		} else if( v.getId() == R.id.pauseButton ) {
			try {
				_mediaPlayer.setDataSource((String) v.getTag() );
				_mediaPlayer.prepare(); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			_playLength = _mediaPlayer.getDuration();
			
			if(!_mediaPlayer.isPlaying()){
				_mediaPlayer.start();
				_buttonPlayPause.setText("Pause");
			} else {
				_mediaPlayer.pause();
				_buttonPlayPause.setText("Play");
			}
			
			progressBarUpdater();
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		_dialog = new Dialog(MusicListActivity.this);
		_dialog.setContentView(R.layout.post_to_fb_dialog);
		_dialog.setTitle("Post To Facebook");

		OnClickListener listener = null;
		
		Object object = _adapter.getItem(position);
		if( object instanceof Artists ) {
			Artists artists = (Artists) object;
			listener = new OnClickListenerForArtists(artists);
			Toast.makeText(MusicListActivity.this, artists.getName(), Toast.LENGTH_SHORT).show();
		} else if( object instanceof Albums ) {
			Albums albums = (Albums) object;
			listener = new OnClickListenerForAlbums(albums);
			Toast.makeText(MusicListActivity.this, albums.getTitle(), Toast.LENGTH_SHORT).show();
		} else if( object instanceof Songs ) {
			Songs songs = (Songs) object;
			listener = new OnClickListenerForSongs(songs);
			Toast.makeText(MusicListActivity.this, songs.getTitle(), Toast.LENGTH_SHORT).show();
			String sampleAddress = songs.getSampleAddress();
			
			if( !sampleAddress.equalsIgnoreCase("na") ) {
				Button sampleMusicButton = (Button) _dialog.findViewById(R.id.sampleMusicButton);
				sampleMusicButton.setVisibility(View.VISIBLE);
				Button pauseButton = (Button) _dialog.findViewById(R.id.pauseButton);
				pauseButton.setTag(songs.getSampleAddress());

				_mediaPlayer = new MediaPlayer();
				_playLength = 0;
				
				_dialog.setOnDismissListener(new OnDismissListener() {
					
					@Override
					public void onDismiss(DialogInterface dialog) {
						if( _mediaPlayer.isPlaying() ) {
							_mediaPlayer.pause();
						}
					}
				});
				sampleMusicButton.setOnClickListener(MusicListActivity.this);
			}
		}

		Button facebookButton = (Button) _dialog.findViewById(R.id.facebookButton);
		facebookButton.setOnClickListener(listener);
		
		_dialog.show();
	}
	
    private void progressBarUpdater() {
		SeekBar seekBar = (SeekBar)_dialog.findViewById(R.id.musicSeekBar);
    	seekBar.setProgress((int)(((float)_mediaPlayer.getCurrentPosition()/_playLength)*100)); // This math construction give a percentage of "was playing"/"song length"
		if (_mediaPlayer.isPlaying()) {
			Runnable runnable = new Runnable() {
		        public void run() {
		        	progressBarUpdater();
				}
		    };
		    _handler.postDelayed(runnable,1000);
    	}
    }
	
	private void showFeedDialog(Bundle bungle) {
		WebDialog dialogBuilder = 
				new WebDialog.Builder(this, getString(R.string.app_id), "feed", bungle)
		.setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void onComplete(Bundle values, FacebookException error) {
                if (error == null) {
                    // When the story is posted, echo the success
                    // and the post Id.
                    final String postId = values.getString("post_id");
                    if (postId != null) {
                        Toast.makeText(MusicListActivity.this,
                            "Posted story, id: "+postId,
                            Toast.LENGTH_SHORT).show();
                    } else {
                        // User clicked the Cancel button
                        Toast.makeText(MusicListActivity.this.getApplicationContext(), 
                            "Publish cancelled", 
                            Toast.LENGTH_SHORT).show();
                    }
                } else if (error instanceof FacebookOperationCanceledException) {
                    // User clicked the "x" button
                    Toast.makeText(MusicListActivity.this.getApplicationContext(), 
                        "Publish cancelled", 
                        Toast.LENGTH_SHORT).show();
                } else {
                    // Generic, ex: network error
                    Toast.makeText(MusicListActivity.this.getApplicationContext(), 
                        "Error posting story", 
                        Toast.LENGTH_SHORT).show();
                }
				
			}
		})
		.build();
		dialogBuilder.show();
	}
	
	class OnClickListenerForArtists implements OnClickListener {
		private Artists _artist;
		
		public OnClickListenerForArtists(Artists artist) {
			_artist = artist;
		}
		
		@Override
		public void onClick(View v) {
		    Bundle bundle = new Bundle();
		    bundle.putString("link", _artist.getDetails());
		    bundle.putString("picture", _artist.getCover());
		    bundle.putString("name", _artist.getName());
		    bundle.putString("caption", "I like " + _artist.getName() + " who is active since " + _artist.getYear());
		    bundle.putString("description", "Genre of Music is: " + _artist.getGenre());
		    bundle.putString("properties",
		    "{\"Look at details\" : {" +
		    "\"text\" : \"here\", " +
		    "\"href\" : \"" + _artist.getDetails() + "\"}}");
		    
		    showFeedDialog(bundle);
		}
	}
	
	class OnClickListenerForAlbums implements OnClickListener {
		private Albums _album;
		
		public OnClickListenerForAlbums(Albums album) {
			_album = album;
		}
		
		@Override
		public void onClick(View v) {
		    Bundle bundle = new Bundle();
		    bundle.putString("link", _album.getDetails());
		    bundle.putString("picture", _album.getCover());
		    bundle.putString("name", _album.getTitle());
		    bundle.putString("caption", "I like " + _album.getTitle() + " released in " + _album.getYear());
		    bundle.putString("description", "Artist: " + _album.getArtist() + " Genre: " + _album.getGenre());
		    bundle.putString("properties",
		    "{\"Look at details\" : {" +
		    "\"text\" : \"here\", " +
		    "\"href\" : \"" + _album.getDetails() + "\"}}");
		    
		    showFeedDialog(bundle);
		}
	}
	
	class OnClickListenerForSongs implements OnClickListener {
		private Songs _song;
		
		public OnClickListenerForSongs(Songs song) {
			_song = song;
		}
		
		@Override
		public void onClick(View v) {
		    Bundle bundle = new Bundle();
		    bundle.putString("link", _song.getDetails());
		    bundle.putString("picture", _song.getSampleImage());
		    bundle.putString("name", _song.getTitle());
		    bundle.putString("caption", "I like " + _song.getTitle() + " composed by " + _song.getComposer());
		    bundle.putString("description", "Performer: " + _song.getPerformer());
		    bundle.putString("properties",
		    "{\"Look at details\" : {" +
		    "\"text\" : \"here\", " +
		    "\"href\" : \"" + _song.getDetails() + "\"}}");
		    
		    showFeedDialog(bundle);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music_search_main, menu);
		return true;
	}
}
