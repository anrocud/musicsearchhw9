package com.bkim.musicsearchhw9;

import java.util.ArrayList;

import com.example.musicsearchhw9.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MusicSearchMainActivity extends Activity implements OnItemSelectedListener, OnClickListener {
	private final String searchTypeDefinition[] = {
			"Artists", "Albums", "Songs"
	};
	
	private int searchTypeNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_music_search_main);
		
		searchTypeNumber = 0;

		ArrayList<String> arraylist = new ArrayList<String>();
		for( int i = 0; i < searchTypeDefinition.length; ++i ) {
			arraylist.add(searchTypeDefinition[i]);
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_spinner_item, arraylist); 
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner sp = (Spinner) findViewById(R.id.searchType);
		sp.setAdapter(adapter);
		sp.setOnItemSelectedListener(this);
		
		Button searchButton = (Button) findViewById(R.id.searchButton);
		searchButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.music_search_main, menu);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		Intent intent = new Intent(getApplicationContext(), MusicListActivity.class);
		EditText searchEditText = (EditText)MusicSearchMainActivity.this.findViewById(R.id.searchText);
		intent.putExtra("searchText", searchEditText.getText().toString());
		intent.putExtra("searchType", searchTypeDefinition[searchTypeNumber]);
		startActivity(intent);
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		searchTypeNumber = arg2;
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

}
