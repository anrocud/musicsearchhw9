package com.bkim.musicsearchhw9.dataclass;

public class Albums {
	private String cover;
	private String title;
	private String artist;
	private String genre;
	private String year;
	private String details;
	
	public String getCover() {
		return cover;
	}
	public Albums setCover(String cover) {
		this.cover = cover;
		return this;
	}
	public String getTitle() {
		return title;
	}
	public Albums setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getArtist() {
		return artist;
	}
	public Albums setArtist(String artist) {
		this.artist = artist;
		return this;
	}
	public String getGenre() {
		return genre;
	}
	public Albums setGenre(String genre) {
		this.genre = genre;
		return this;
	}
	public String getYear() {
		return year;
	}
	public Albums setYear(String year) {
		this.year = year;
		return this;
	}
	public String getDetails() {
		return details;
	}
	public Albums setDetails(String details) {
		this.details = details;
		return this;
	}
	
}
