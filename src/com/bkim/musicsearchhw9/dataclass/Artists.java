package com.bkim.musicsearchhw9.dataclass;

public class Artists {
	private String cover;
	private String name;
	private String genre;
	private String year;
	private String details;
	
	public String getCover() {
		return cover;
	}
	public Artists setCover(String cover) {
		this.cover = cover;
		return this;
	}
	public String getName() {
		return name;
	}
	public Artists setName(String name) {
		this.name = name;
		return this;
	}
	public String getGenre() {
		return genre;
	}
	public Artists setGenre(String genre) {
		this.genre = genre;
		return this;
	}
	public String getYear() {
		return year;
	}
	public Artists setYear(String year) {
		this.year = year;
		return this;
	}
	public String getDetails() {
		return details;
	}
	public Artists setDetails(String details) {
		this.details = details;
		return this;
	}
}
