package com.bkim.musicsearchhw9.dataclass;

public class Songs {
	private String sampleAddress;
	private String sampleImage;
	private String title;
	private String performer;
	private String composer;
	private String details;
	
	public String getSampleAddress() {
		return sampleAddress;
	}
	public Songs setSampleAddress(String sampleAddress) {
		this.sampleAddress = sampleAddress;
		return this;
	}
	public String getSampleImage() {
		return sampleImage;
	}
	public Songs setSampleImage(String sampleImage) {
		this.sampleImage = sampleImage;
		return this;
	}
	public String getTitle() {
		return title;
	}
	public Songs setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getPerformer() {
		return performer;
	}
	public Songs setPerformer(String performer) {
		this.performer = performer;
		return this;
	}
	public String getComposer() {
		return composer;
	}
	public Songs setComposer(String composer) {
		this.composer = composer;
		return this;
	}
	public String getDetails() {
		return details;
	}
	public Songs setDetails(String details) {
		this.details = details;
		return this;
	}
}
